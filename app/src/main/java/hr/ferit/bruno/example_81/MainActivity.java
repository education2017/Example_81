package hr.ferit.bruno.example_81;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    @BindView(R.id.lvItemsList) ListView lvItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.initialize();
    }

    private void initialize() {
        int elementCount = getResources().getInteger(R.integer.intElementCount);
        ArrayList<String> elements = this.createElements(elementCount);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elements);
        lvItemsList.setAdapter(adapter);
    }

    private ArrayList<String> createElements(int count) {
        ArrayList<String> elements = new ArrayList<>(count);
        for (int i=0; i<count; i++){
            String element = "Element" + i;
            elements.add(i, element);
        }
        return elements;
    }
}
